import glob
import ast

class TestProcessor:
    def __init__(self, config, connector, logger):
        """This function is a constructor for TestProcessor.
        """
        self.config = config
        self.connector = connector
        self.logger = logger

    def process(self):
        """This function checks and runs the tests in folder.
        """
        test_data_files = self.check_test_folder()

        for f in test_data_files:
            self.do_testing(f)

    def check_test_folder(self):
        """This function returns the list of files eith tests.
        """
        test_data_folder = self.config.get_test_data_folder()
        return [f for f in glob.glob(test_data_folder + '/*.json', recursive=True)]

    def do_testing(self, file_name):
        """This function runs the query and check the expected result.

            Parameters:
            file_name (str): File name.
        """
        self.logger.start_test(file_name)

        with open(file_name) as f:
            test_data = eval(f.read())

        for test in test_data["tests"]:
            self.logger.start_case(test["name"])

            query = test["query"]
            expected_result = test["expected"] if type(test["expected"]) is int else ast.literal_eval(test["expected"].replace("\'", "\""))
            actual_result = self.connector.execute(query)
            
            if actual_result == expected_result:
                self.logger.add_pass(query, actual_result)
            else:
                self.logger.add_fail(query, actual_result, expected_result)
