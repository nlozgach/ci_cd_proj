class Result:
    def __init__(self):
        """This function is a constructor for Result logging.
        """
        self.result_file = open('results/result.log', 'w')

    def start_test(self, file_name):
        """This function logs the start of the test.
        """
        self.result_file.write('\n\nTesting of {} was started\n'.format(file_name))

    def start_case(self, test_name):
        """This function logs the name of the test.
        """
        self.result_file.write('\n\nTest case "{}"\n'.format(test_name))
    
    def add_pass(self, query, actual_result):
        """This function logs the pass of the test.
        """
        self.result_file.write('\nPASS. Result is {0} as expected'
                               '\n\nQuery: {1}'.format(actual_result, query))
    
    def add_fail(self, query, actual_result, expected_result):
        """This function logs the fail of the test.
        """
        self.result_file.write('\FAIL. Result is "{0}", but expected "{1}"'
                               '\n\nQuery: {2}'.format(actual_result, expected_result, query))

    def finish_test(self):
        """This function logs the finish of the test.
        """
        self.result_file.close()
