import pyodbc

class Connector:
    def __init__(self, database_url, server_name_url):
        """This function is a constructor for Connector.
        """
        drivers = pyodbc.drivers()
        conn = pyodbc.connect('Driver={0};'
                      'Server={1};'
                      'Database={2};'
                      'Trusted_Connection=yes;'.format(drivers[0], server_name_url, database_url))
        self.cursor = conn.cursor()

    def execute(self, query):
        """This function runs the query and returns the result.

            Parameters:
            query (str): SQL query for DataBase.
        """
        self.cursor.execute(query)
        result_row = {}
        row = self.cursor.fetchone()
        i = 1
        while row is not None:
            result_row[i] = row[len(row) - 1]
            row = self.cursor.fetchone()
            i = i + 1
        return result_row