class Configurator:
    def __init__(self, environment):
        """This function is a constructor for Configurator.
        """
        with open('config.json') as f:
            self.config = eval(f.read())
        self.config = self.config[environment]

    def get_database_url(self):
        """This function return the config for database.
        """
        return self.config["database"]

    def get_server_name_url(self):
        """This function return the config for server_name.
        """
        return self.config["server_name"]

    def get_test_data_folder(self):
        """This function return the config for tests_folder.
        """
        return self.config["tests_folder"]
