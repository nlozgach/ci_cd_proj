from configurator import Configurator
from connector import Connector
from test_processor import TestProcessor
from resulting import Result
from sys import argv


def run():
    """This function run the tests with config for environment.
    """
    config = Configurator(argv[1])
    server_name_url = config.get_server_name_url()
    database_url = config.get_database_url()

    conn = Connector(database_url, server_name_url)

    logger = Result()

    test_proc = TestProcessor(config, conn, logger)
    test_proc.process()

    logger.finish_test()

if __name__ == '__main__':
    run()